from django.urls import path
from accounts.views import signup, user_login, logout_view

urlpatterns = [
    path("logout/", logout_view, name="logout"),
    path("login/", user_login, name="login"),
    path("signup/", signup, name="signup"),
]
